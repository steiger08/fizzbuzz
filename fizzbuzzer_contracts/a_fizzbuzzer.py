from abc import ABC, abstractclassmethod

class AFizzBuzzer(ABC) :

    @abstractclassmethod
    def doFizzBuzz(self, number: int) -> str:
        pass