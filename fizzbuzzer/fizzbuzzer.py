from fizzbuzzer_contracts.a_fizzbuzzer import AFizzBuzzer 

class FizzBuzzer(AFizzBuzzer) :
    def doFizzBuzz(self, number: int) -> str:
        resultText: str = ''
        
        if(number < 0) :
            raise ValueError("Value out of range")

        if(number % 3 == 0):
            resultText = "FIZZ"

        if(number % 5 == 0):
            resultText += "BUZZ"

        if len(resultText) == 0:
            resultText = str(number)

        return resultText