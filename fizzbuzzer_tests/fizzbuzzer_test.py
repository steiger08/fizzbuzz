import unittest
from fizzbuzzer_contracts.a_fizzbuzzer import AFizzBuzzer
from fizzbuzzer.fizzbuzzer import FizzBuzzer

class FizzBuzzerTest(unittest.TestCase) :
    
    fizzBuzzer: AFizzBuzzer

    def setUp(self):
        self.fizzBuzzer = FizzBuzzer()

    def test_doFizzBuzz1returns1(self):
        result = self.fizzBuzzer.doFizzBuzz(1)
        self.assertEqual(result, "1")

    def test_doFizzBuzz2returns2(self):
        result = self.fizzBuzzer.doFizzBuzz(2)
        self.assertEqual(result, "2")

    def test_doFizzBuzz3returnsFIZZ(self):
        result = self.fizzBuzzer.doFizzBuzz(3)
        self.assertEqual(result, "FIZZ")

    def test_doFizzBuzz15returnsBUZZ(self):
        result = self.fizzBuzzer.doFizzBuzz(5)
        self.assertEqual(result, "BUZZ")

    def test_doFizzBuzz15returnsFIZZBUZZ(self):
        result = self.fizzBuzzer.doFizzBuzz(15)
        self.assertEqual(result, "FIZZBUZZ")

    def test_doFizzBuzzThrowsOnValue(self):
        self.assertRaises(ValueError, self.fizzBuzzer.doFizzBuzz, -1)

    def test_doFizzBuzzThrowsOnType(self):
        self.assertRaises(Exception, self.fizzBuzzer.doFizzBuzz, "A string instead of an integer")